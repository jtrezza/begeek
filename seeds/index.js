'use strict';

var fs        = require('fs');
var path      = require('path');
var basename  = path.basename(module.filename);
var env       = process.env.NODE_ENV || 'development';
var models  = require('../models');

console.log('Inserting data...');

fs
    .readdirSync(__dirname + '/' + env)
    .filter(function(file) {
        return (file.indexOf('.') !== 0) && (file !== basename);
    })
    .forEach(function(file) {
        if (file.slice(-3) !== '.js') return;
        var seed = require(path.join(__dirname, env, file));
        seed['data'].forEach(function(data){
            models[seed.model].create(data);
        });
    });
//TODO: Clean all tables before fill them. Do that using Gulp.