'use strict'

const faker = require('faker'),
      data = [];

faker.locale = 'es';
exports.model = 'User';

for(var i = 0; i < 40; i++){
  data.push({
    names: faker.name.firstName(),
    surnames: faker.name.lastName() + ' ' + faker.name.lastName(),
    gender_id: Math.floor((Math.random() * 4) + 1),
    email: faker.internet.email(),
    password: faker.internet.password(),
    role_id: Math.floor((Math.random() * 2) + 1),
    remember_token:faker.internet.password(),
    active: faker.random.boolean()
  });
}

exports.data = data;