'use strict'

exports.model = 'Role';

exports.data = [
    {name: 'Administrator', permissions: [1, 2, 4]},
    {name: 'Customer', permissions: [8, 16, 32]}
];