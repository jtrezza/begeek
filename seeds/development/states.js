'use strict'

exports.model = 'State';

exports.data = [
    {code: '05', name: 'ANTIOQUIA'},
    {code: '08', name: 'ATLANTICO'},
    {code: '13', name: 'BOLIVAR'},
    {code: '15', name: 'BOYACA'},
    {code: '17', name: 'CALDAS'},
    {code: '18', name: 'CAQUETA'},
    {code: '19', name: 'CAUCA'},
    {code: '20', name: 'CESAR'},
    {code: '23', name: 'CORDOBA'},
    {code: '25', name: 'CUNDINAMARCA'},
    {code: '27', name: 'CHOCO'},
    {code: '41', name: 'HUILA'},
    {code: '44', name: 'LA GUAJIRA'},
    {code: '47', name: 'MAGDALENA'},
    {code: '50', name: 'META'},
    {code: '52', name: 'NARIÑO'},
    {code: '54', name: 'NORTE DE SANTANDER'},
    {code: '63', name: 'QUINDIO'},
    {code: '66', name: 'RISARALDA'},
    {code: '68', name: 'SANTANDER'},
    {code: '70', name: 'SUCRE'},
    {code: '73', name: 'TOLIMA'},
    {code: '76', name: 'VALLE DEL CAUCA'},
    {code: '81', name: 'ARAUCA'},
    {code: '85', name: 'CASANARE'},
    {code: '86', name: 'PUTUMAYO'},
    {code: '88', name: 'SAN ANDRES'},
    {code: '91', name: 'AMAZONAS'},
    {code: '94', name: 'GUANIA'},
    {code: '95', name: 'GUAVIARE'},
    {code: '97', name: 'VAUPES'},
    {code: '99', name: 'VICHADA'}
];