'use strict'

exports.model = 'Gender';

exports.data = [
    {code:'M', name:'HOMBRE'},
    {code:'F', name:'MUJER'},
    {code:'O', name:'OTRO'},
    {code:'N', name:'PREFIERO NO DECIRLO'}
];