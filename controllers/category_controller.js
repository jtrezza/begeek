'use strict'
const models = require('../models');
const util = require('util');

exports.className = 'CategoryController';

exports.create = function(req, res){

  let category = req.body;

  models['Category'].create({
    parent_id: category.parent_id,
    name: category.name,
    slug: category.slug,
    description: category.description,
    short_description: category.short_description,
    seo_title: category.seo_title,
    seo_description: category.seo_description
  }).then(function(category){
    //Creado con éxito
    let response = {
      data: {
        id: category.id,
        parent_id: category.parent_id,
        name: category.name,
        slug: category.slug,
        description: category.description,
        short_description: category.short_description,
        seo_title: category.seo_title,
        seo_description: category.seo_description
      }
    };
    res.status(201).end(JSON.stringify(response));

  }).catch(function(error){
    //Errores de validación
    let response = {
      errors: [{title: 'Database error', detail: error.message}]
    };
    res.status(400).end(JSON.stringify(response));
  }).error(function(error){
    //Cualquier otro error
    let response = {
      errors: [{title: 'Internal error', detail: error.message}]
    };
    res.status(500).end(JSON.stringify(response));
  });

};

exports.show = function(req, res){

  let category_id = req.params.id;

  models['Category'].findById(category_id, {
    include: [
      {
        model: models.Product,
        include: [
          {
            model: models.ProductImage
          }
        ]
      }
    ]
  }).then(function(category){
    if (category === null){
      let response = {
        errors: [{title: 'Not found', detail: `The resource with ID ${category_id} does not exist`}]
      };
      res.status(404).end(JSON.stringify(response));
    } else {

      let data = generateResponseData(category);
      let response = {
        data: data
      };

      res.status(200).end(JSON.stringify(response));
    }

  }).error(function(error){
    //Cualquier otro error
    let response = {
      errors: [{title: 'Database error', detail: error.message}]
    };
    res.status(500).end(JSON.stringify(response));
  });
};

exports.update = function(req, res){

  let category_id = req.params.id;

  models['Category'].findById(category_id).then(function(category){
    if (category === null){
      let response = {
        errors: [{title: 'Not found', detail: `The resource with ID ${category_id} does not exist`}]
      };
      res.status(404).end(JSON.stringify(response));
    } else {

      let new_values = req.body;
      for(var key in new_values){
        category[key] = new_values[key];
      }

      category.save().then(function(result){

        let data = generateResponseData(result);
        let response = {
          data: data
        };
        res.status(200).end(JSON.stringify(response));

      }).catch(function(error){
        //Errores de validación
        let response = {
          errors: [{title: 'Database error', detail: error.message}]
        };
        res.status(400).end(JSON.stringify(response));
      }).error(function(error){
        //Cualquier otro error
        let response = {
          errors: [{title: 'Internal error', detail: error.message}]
        };
        res.status(500).end(JSON.stringify(response));
      });
    }

  });
};

exports.delete = function(req, res){

  let category_id = req.params.id;

  models['Category'].findById(category_id).then(function(category){
    if (category === null){
      let response = {
        errors: [{title: 'Not found', detail: `The resource with ID ${category_id} does not exist`}]
      };
      res.status(404).end(JSON.stringify(response));
    } else {

      category.destroy();
      let response = {
        meta: {
          title: "success",
          message: "deletion successfully"
        }
      };
      res.status(200).end(JSON.stringify(response));
    }

  }).error(function(error){
    //Cualquier otro error
    let response = {
      errors: [{title: 'Internal error', detail: error.message}]
    };
    res.status(500).end(JSON.stringify(response));
  });
};

exports.list = function(req, res){

  models['Category'].findAll({
    include: [
      {
        model: models.Product,
        include: [
          {
            model: models.ProductImage
          }
        ]
      }
    ]
  })
    .then(function(categories) {

      let data = categories.map(function(e) {
        return generateResponseData(e);
      });

      let response = {
        data: data
      };

      res.status(200).end(JSON.stringify(response));

    }).error(function(error){
      //Cualquier otro error
      let response = {
        errors: [{title: 'Internal error', detail: error.message}]
      };
      res.status(500).end(JSON.stringify(response));
    });

};

exports.uploadImage = function(req, res){
  //TODO: Implement file uploads
  res.send('Uploading image');
};

function generateResponseData(response){

  let products = response.Products ? response['Products'].map(function(p){

    let images = p.ProductImages ? p['ProductImages'].map(function(i){
      return {
        id: i.id,
        product_id: i.product_id,
        filename: i.filename,
        title: i.title,
        description: i.description,
        order: i.order,
        main: i.main
      };
    }) : [];

    return {
      id: p.id,
      sku: p.sku,
      name: p.name,
      slug: p.slug,
      vendor_id: p.vendor_id,
      description: p.description,
      short_description: p.short_description,
      price: p.price,
      sale_price: p.sale_price,
      total_stock: p.total_stock,
      visibility: p.visibility,
      seo_title: p.seo_title,
      seo_description: p.seo_description,
      unit_of_measurement_id: p.unit_of_measurement_id,
      weight: p.weight,
      images: images
    };

  }) : [];

  return {
    id: response.id,
    parent_id: response.parent_id,
    name: response.name,
    slug: response.slug,
    description: response.description,
    short_description: response.short_description,
    seo_title: response.seo_title,
    seo_description: response.seo_description,
    products: products
  };
}