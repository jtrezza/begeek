'use strict'
const models = require('../models'),
  multiparty = require('multiparty'),
  path = require('path'),
  util = require('util');

exports.className = 'ProductController';


exports.create = function(req, res){

  let body = req.body,
      new_values = {};

  for(var key in body){
    new_values[key] = body[key];
  }

  models['Product'].create(new_values).then(function(product){

    let data = generateResponseData(product);

    //Creado con éxito
    let response = {
      data: data
    };
    res.status(201).end(JSON.stringify(response));

  }).catch(function(error){
    //Errores de validación
    let response = {
      errors: [{title: 'Database error', detail: error.message}]
    };
    res.status(400).end(JSON.stringify(response));
  }).error(function(error){
    //Cualquier otro error
    let response = {
      errors: [{title: 'Internal error', detail: error.message}]
    };
    res.status(500).end(JSON.stringify(response));
  });

};

exports.show = function(req, res){

  let product_id = req.params.id;

  models['Product'].findById(product_id, {
    include: [
      {model: models.ProductImage},
      {model: models.Category}
    ]
  }).then(function(product){
    if (product === null){
      let response = {
        errors: [{title: 'Not found', detail: `The resource with ID ${product_id} does not exist`}]
      };
      res.status(404).end(JSON.stringify(response));
    } else {

      let data = generateResponseData(product);

      let response = {
        data: data
      };
      res.status(200).end(JSON.stringify(response));
    }

  }).error(function(error){
    //Cualquier otro error
    let response = {
      errors: [{title: 'Internal error', detail: error.message}]
    };
    res.status(500).end(JSON.stringify(response));
  });
};

exports.update = function(req, res){

  let product_id = req.params.id;

  models['Product'].findById(product_id).then(function(product){
    if (product === null){
      let response = {
        errors: [{title: 'Not found', detail: `The resource with ID ${product_id} does not exist`}]
      };
      res.status(404).end(JSON.stringify(response));
    } else {

      let new_values = req.body;
      for(var key in new_values){
        product[key] = new_values[key];
      }

      product.save().then(function(result){

        let data = generateResponseData(result);

        let response = {
          data: data
        };
        res.status(200).end(JSON.stringify(response));

      }).catch(function(error){
        //Errores de validación
        let response = {
          errors: [{title: 'Database error', detail: error.message}]
        };
        res.status(400).end(JSON.stringify(response));
      }).error(function(error){
        //Cualquier otro error
        let response = {
          errors: [{title: 'Internal error', detail: error.message}]
        };
        res.status(500).end(JSON.stringify(response));
      });
    }

  });
};

exports.delete = function(req, res){

  let product_id = req.params.id;

  models['Product'].findById(product_id).then(function(product){
    if (product === null){
      let response = {
        errors: [{title: 'Not found', detail: `The resource with ID ${product_id} does not exist`}]
      };
      res.status(404).end(JSON.stringify(response));
    } else {

      product.destroy().then(function(){
        let response = {
          meta: {
            title: "success",
            message: "deletion successfully"
          }
        };
        res.status(200).end(JSON.stringify(response));
      });

    }

  }).error(function(error){
    //Cualquier otro error
    let response = {
      errors: [{title: 'Internal error', detail: error.message}]
    };
    res.status(500).end(JSON.stringify(response));
  });
};

exports.list = function(req, res){

  models['Product'].findAll({
    include: [
      {model: models.ProductImage},
      {model: models.Category}
    ]
  })
    .then(function(products) {

      let data = products.map(function(e) {
        return generateResponseData(e);
      });

      let response = {
        data: data
      };

      res.status(200).end(JSON.stringify(response));

    }).error(function(error){
      //Cualquier otro error
      let response = {
        errors: [{title: 'Internal error', detail: error.message}]
      };
      res.status(500).end(JSON.stringify(response));
    });

};

function generateResponseData(response){

  let categories = response.Categories ? response['Categories'].map(function(c){
    return {
      id: c.id,
      parent_id: c.parent_id,
      name: c.name,
      slug: c.slug,
      description: c.description,
      short_description: c.short_description,
      seo_title: c.seo_title,
      seo_description: c.seo_description,
      visibility: c.visibility
    };
  }) : [];

  let images = response.ProductImages ? response['ProductImages'].map(function(i){
    return {
      id: i.id,
      product_id: i.product_id,
      filename: i.filename,
      title: i.title,
      description: i.description,
      order: i.order,
      main: i.main
    };
  }) : [];

  return {
    id: response.id,
    sku: response.sku,
    name: response.name,
    slug: response.slug,
    vendor_id: response.vendor_id,
    description: response.description,
    short_description: response.short_description,
    price: response.price,
    sale_price: response.sale_price,
    total_stock: response.total_stock,
    visibility: response.visibility,
    seo_title: response.seo_title,
    seo_description: response.seo_description,
    unit_of_measurement_id: response.unit_of_measurement_id,
    weight: response.weight,
    combinations: response.combinations,
    reviews: response.reviews,
    categories: categories,
    images: images
  };
}
