'use strict'
const models = require('../models'),
  multiparty = require('multiparty'),
  path = require('path'),
  fs = require('fs'),
  util = require('util');

exports.className = 'ProductImageController';

exports.create = function(req, res){

  let product_id = req.params.id;
  let mb = 4 * 1024 * 1024;
  let form = new multiparty.Form({
    uploadDir: path.join(__dirname, '..', 'public', 'uploads'),
    maxFilesSize: mb
  });

  form.parse(req, function(err, fields, files) {

    if(!files || !files['image']){
      let response = {
        errors: [{title: 'Request error', detail: 'There are no files or they are too big'}]
      };
      res.status(400).end(JSON.stringify(response));
    }

    let new_values = {};

    for(var key in fields){
      new_values[key] = fields[key][0];
    }

    new_values.product_id = product_id;
    new_values.filename = path.basename(files['image'][0].path);

    models['ProductImage'].create(new_values).then(function(productImage){

      let data = generateResponseData(productImage);

      //Creado con éxito
      let response = {
        data: data
      };
      res.status(201).end(JSON.stringify(response));

    }).catch(function(error){
      //Errores de validación
      let response = {
        errors: [{title: 'Database error', detail: error.message}]
      };
      res.status(400).end(JSON.stringify(response));
    }).error(function(error){
      //Cualquier otro error
      let response = {
        errors: [{title: 'Internal error', detail: error.message}]
      };
      res.status(500).end(JSON.stringify(response));
    });

  });
};

exports.show = function(req, res){

  let image_id = req.params.id;

  models['ProductImage'].findById(image_id).then(function(productImage){
    if (productImage === null){
      let response = {
        errors: [{title: 'Not found', detail: `The resource with ID ${image_id} does not exist`}]
      };
      res.status(404).end(JSON.stringify(response));
    } else {

      let data = generateResponseData(productImage);

      let response = {
        data: data
      };
      res.status(200).end(JSON.stringify(response));
    }

  }).error(function(error){
    //Cualquier otro error
    let response = {
      errors: [{title: 'Internal error', detail: error.message}]
    };
    res.status(500).end(JSON.stringify(response));
  });
};

exports.update = function(req, res){

  let image_id = req.params.id;

  models['ProductImage'].findById(image_id).then(function(productImage){
    if (productImage === null){
      let response = {
        errors: [{title: 'Not found', detail: `The resource with ID ${image_id} does not exist`}]
      };
      res.status(404).end(JSON.stringify(response));
    } else {

      let new_values = req.body;
      delete new_values['product_id'];
      delete new_values['filename'];
      for(var key in new_values){
        productImage[key] = new_values[key];
      }

      productImage.save().then(function(result){

        let data = generateResponseData(result);

        let response = {
          data: data
        };
        res.status(200).end(JSON.stringify(response));

      }).catch(function(error){
        //Errores de validación
        let response = {
          errors: [{title: 'Database error', detail: error.message}]
        };
        res.status(400).end(JSON.stringify(response));
      }).error(function(error){
        //Cualquier otro error
        let response = {
          errors: [{title: 'Internal error', detail: error.message}]
        };
        res.status(500).end(JSON.stringify(response));
      });
    }

  });
};

exports.delete = function(req, res){

  let image_id = req.params.id;

  models['ProductImage'].findById(image_id).then(function(productImage){
    if (productImage === null){
      let response = {
        errors: [{title: 'Not found', detail: `The resource with ID ${image_id} does not exist`}]
      };
      res.status(404).end(JSON.stringify(response));

    } else {

      fs.unlink(path.join(__dirname, '..', 'public', 'uploads', productImage.filename), function (err) {
        if (err) throw err;

        productImage.destroy().then(function(){
          let response = {
            meta: {
              title: "success",
              message: "deletion successfully"
            }
          };
          res.status(200).end(JSON.stringify(response));
        });
      });


    }

  }).error(function(error){
    //Cualquier otro error
    let response = {
      errors: [{title: 'Internal error', detail: error.message}]
    };
    res.status(500).end(JSON.stringify(response));
  });
};

exports.list = function(req, res){

  models['ProductImage'].findAll()
    .then(function(images) {
      let data = [];

      images.forEach(function(e) {
        data.push(generateResponseData(e));
      });

      let response = {
        meta: {test: 'prueba'},
        data: data
      };

      res.status(200).end(JSON.stringify(response));

    }).error(function(error){
      //Cualquier otro error
      let response = {
        errors: [{title: 'Internal error', detail: error.message}]
      };
      res.status(500).end(JSON.stringify(response));
    });

};

function generateResponseData(response){
  return {
    id: response.id,
    product_id: response.product_id,
    filename: response.filename,
    title: response.title,
    description: response.description,
    order: response.order,
    main: response.main
  }
}