'use strict'
const models = require('../models');

exports.className = 'VendorController';


exports.create = function(req, res){

  let body = req.body,
    new_values = {};

  for(var key in body){
    new_values[key] = body[key];
  }

  models['Vendor'].create(new_values).then(function(vendor){

    let data = generateResponseData(vendor);

    //Creado con éxito
    let response = {
      data: data
    };
    res.status(201).end(JSON.stringify(response));

  }).catch(function(error){
    //Errores de validación
    let response = {
      errors: [{title: 'Database error', detail: error.message}]
    };
    res.status(400).end(JSON.stringify(response));
  }).error(function(error){
    //Cualquier otro error
    let response = {
      errors: [{title: 'Internal error', detail: error.message}]
    };
    res.status(500).end(JSON.stringify(response));
  });

};

exports.show = function(req, res){

  let vendor_id = req.params.id;

  models['Vendor'].findById(vendor_id).then(function(vendor){
    if (vendor === null){
      let response = {
        errors: [{title: 'Not found', detail: `The resource with ID ${vendor_id} does not exist`}]
      };
      res.status(404).end(JSON.stringify(response));
    } else {

      let data = generateResponseData(vendor);

      let response = {
        data: data
      };
      res.status(200).end(JSON.stringify(response));
    }

  }).error(function(error){
    //Cualquier otro error
    let response = {
      errors: [{title: 'Internal error', detail: error.message}]
    };
    res.status(500).end(JSON.stringify(response));
  });
};

exports.update = function(req, res){

  let vendor_id = req.params.id;

  models['Vendor'].findById(vendor_id).then(function(vendor){
    if (vendor === null){
      let response = {
        errors: [{title: 'Not found', detail: `The resource with ID ${vendor_id} does not exist`}]
      };
      res.status(404).end(JSON.stringify(response));
    } else {

      let new_values = req.body;
      for(var key in new_values){
        vendor[key] = new_values[key];
      }

      vendor.save().then(function(result){

        let data = generateResponseData(result);

        let response = {
          data: data
        };
        res.status(200).end(JSON.stringify(response));

      }).catch(function(error){
        //Errores de validación
        let response = {
          errors: [{title: 'Database error', detail: error.message}]
        };
        res.status(400).end(JSON.stringify(response));
      }).error(function(error){
        //Cualquier otro error
        let response = {
          errors: [{title: 'Internal error', detail: error.message}]
        };
        res.status(500).end(JSON.stringify(response));
      });
    }

  });
};

exports.delete = function(req, res){

  let vendor_id = req.params.id;

  models['Vendor'].findById(vendor_id).then(function(vendor){
    if (vendor === null){
      let response = {
        errors: [{title: 'Not found', detail: `The resource with ID ${vendor_id} does not exist`}]
      };
      res.status(404).end(JSON.stringify(response));
    } else {

      vendor.destroy().then(function(){
        let response = {
          meta: {
            title: "success",
            message: "deletion successfully"
          }
        };
        res.status(200).end(JSON.stringify(response));
      });

    }

  }).error(function(error){
    //Cualquier otro error
    let response = {
      errors: [{title: 'Internal error', detail: error.message}]
    };
    res.status(500).end(JSON.stringify(response));
  });
};

exports.list = function(req, res){

  models['Vendor'].findAll()
    .then(function(vendors) {
      let data = [];

      vendors.forEach(function(e) {
        data.push(generateResponseData(e));
      });

      let response = {
        data: data
      };

      res.status(200).end(JSON.stringify(response));

    }).error(function(error){
      //Cualquier otro error
      let response = {
        errors: [{title: 'Internal error', detail: error.message}]
      };
      res.status(500).end(JSON.stringify(response));
    });

};

exports.uploadImage = function(req, res){
  //TODO: Implement file uploads
  res.send('Uploading image');
};

function generateResponseData(response){

  let data= {
    id: response.id,
    name: response.name
  };

  return data;
}
