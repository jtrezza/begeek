'use strict'

exports.className = 'UserController';

exports.create = function(req, res){
    res.send('Creando usuario');
};

exports.show = function(req, res){
    res.send('mostrando la id '+ req.params.id);
};

exports.update = function(req, res){
    res.send('Updating id '+ req.params.id);
};

exports.delete = function(req, res){
    res.send('Deleting id '+ req.params.id);
};

exports.list = function(req, res){
    res.send('Listing users from controller');
};