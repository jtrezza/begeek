'use strict'

const express = require('express'),
      router = express.Router(),
      controllers = require('../../../controllers');

//Validating all IDs to be Integers
router.param('id', function(req, res, next, id){
  var regExp = /^\d+$/;
  if (!id.match(regExp)) {
    let response = {
      errors: [{title: 'Request error', detail: 'The ID must be an Integer'}]
    };
    res.status(400).end(JSON.stringify(response));
  } else {
    next();
  }
});

//Users routes
router.post('/users', controllers['UserController'].create);

router.get('/users/:id', controllers['UserController'].show);

router.patch('/users/:id', controllers['UserController'].update);

router.delete('/users/:id', controllers['UserController'].delete);

router.get('/users', controllers['UserController'].list);

//Categories routes
router.post('/categories', controllers['CategoryController'].create);

router.get('/categories/:id', controllers['CategoryController'].show);

router.patch('/categories/:id', controllers['CategoryController'].update);

router.delete('/categories/:id', controllers['CategoryController'].delete);

router.get('/categories', controllers['CategoryController'].list);

router.put('/categories/:id/image', controllers['CategoryController'].uploadImage);

//Products routes
router.post('/products', controllers['ProductController'].create);

router.get('/products/:id', controllers['ProductController'].show);

router.patch('/products/:id', controllers['ProductController'].update);

router.delete('/products/:id', controllers['ProductController'].delete);

router.get('/products', controllers['ProductController'].list);

//Product images routes
router.post('/products/:id/images', controllers['ProductImageController'].create);

router.get('/productimages/:id', controllers['ProductImageController'].show);

router.patch('/productimages/:id', controllers['ProductImageController'].update);

router.delete('/productimages/:id', controllers['ProductImageController'].delete);

router.get('/products/:id/images', controllers['ProductImageController'].list);

//Vendors routes
router.post('/vendors', controllers['VendorController'].create);

router.get('/vendors/:id', controllers['VendorController'].show);

router.patch('/vendors/:id', controllers['VendorController'].update);

router.delete('/vendors/:id', controllers['VendorController'].delete);

router.get('/vendors', controllers['VendorController'].list);

router.post('/vendors/:id/image', controllers['VendorController'].uploadImage);

//Exporting all routes
module.exports = router;