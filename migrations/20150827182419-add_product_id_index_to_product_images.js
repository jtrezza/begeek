'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {

    return queryInterface.addIndex('product_images', ['product_id'], {
      indexName: 'product_images_product_id_index',
      indexType: 'BTREE'}
    );
  },

  down: function (queryInterface, Sequelize) {

    return queryInterface.removeIndex('product_images', 'product_images_product_id_index');
  }
};
