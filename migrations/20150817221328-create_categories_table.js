'use strict';

module.exports = {

  up: function (queryInterface, Sequelize) {

    return queryInterface.createTable('categories', {
      id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      parent_id: Sequelize.INTEGER,
      name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      slug: {
        type: Sequelize.STRING,
        allowNull: false
      },
      description: Sequelize.TEXT,
      short_description: Sequelize.STRING,
      seo_title: Sequelize.STRING,
      seo_description: Sequelize.STRING,
      visibility: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
        allowNull: false
      },
      created_at: {
        type: Sequelize.DATE
      },
      updated_at: {
        type: Sequelize.DATE
      },
      deleted_at: {
        type: Sequelize.DATE
      }
    });
  },

  down: function (queryInterface, Sequelize) {

    return queryInterface.dropTable('categories');
  }
};
