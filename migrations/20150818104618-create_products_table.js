'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('products', {
      id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      sku: Sequelize.STRING,
      name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      slug: {
        type: Sequelize.STRING,
        allowNull: false
      },
      vendor_id: Sequelize.INTEGER,
      description: Sequelize.TEXT,
      short_description: Sequelize.STRING,
      price: Sequelize.INTEGER,
      sale_price: Sequelize.INTEGER,
      total_stock: Sequelize.INTEGER,
      visibility: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
        allowNull: false
      },
      seo_title: Sequelize.STRING,
      seo_description: Sequelize.STRING,
      unit_of_measurement_id: Sequelize.INTEGER,
      weight: Sequelize.FLOAT, //To calculate shipping cost
      combinations: Sequelize.JSON,
      reviews: Sequelize.JSON,
      created_at: {
        type: Sequelize.DATE
      },
      updated_at: {
        type: Sequelize.DATE
      },
      deleted_at: {
        type: Sequelize.DATE
      }
    });

  },

  down: function (queryInterface, Sequelize) {

    return queryInterface.dropTable('products');
  }
};
