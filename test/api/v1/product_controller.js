'use strict'

const test = require('tape'),
      controllers = require('../../../controllers'),
      request = require('request');

test("Testing create, update, list and delete in products", function(t){

  let name = 'Test product',
      slug = 'test-product';

  //Testing create operation
  request.post({url: 'http://localhost:3000/api/v1/products',
    form: {name: name, slug: slug}
  }, function(err, response, body){

    if (err) t.fail(err);
    let parsedBody = JSON.parse(body);

    t.equal(response.statusCode, 201, "Status code should be 201 'created'");
    t.ok(Number.isInteger(parsedBody.data.id), 'New ID must be an Integer');
    t.equal(parsedBody.data.name, name, 'Field name has been saved properly');
    t.equal(parsedBody.data.slug, slug, 'Field slug has been saved properly');

    //Testing update inside the create callback
    let new_name = 'New Name', new_slug = 'New Slug';
    request.patch({url: `http://localhost:3000/api/v1/products/${parsedBody.data.id}`,
      form: {name: new_name, slug: new_slug}
    }, function(err, response, body){
      if (err) t.fail(err);
      let parsedBody = JSON.parse(body);

      t.equal(response.statusCode, 200, "Status code should be 200 'OK' on update");
      t.equal(parsedBody.data.name, new_name, 'Field name has been updated properly');
      t.equal(parsedBody.data.slug, new_slug, 'Field slug has been updated properly');

      //Testing get inside the update callback
      request.get({url: `http://localhost:3000/api/v1/products/${parsedBody.data.id}`},
        function(err, response, body){
          if (err) t.fail(err);
          let parsedBody = JSON.parse(body);

          t.equal(response.statusCode, 200, "Product retrieved successfully");
          t.equal(parsedBody.data.name, new_name, 'Field name has been retrieved properly');
          t.equal(parsedBody.data.slug, new_slug, 'Field slug has been retrieved properly');

          //Testing delete inside the get callback
          request.del({url: `http://localhost:3000/api/v1/products/${parsedBody.data.id}`},
            function(err, response, body){
              if (err) t.fail(err);

              t.equal(response.statusCode, 200, "Status code should be 200 'OK' on delete");

              //Testing delete on a non existing resource
              request.del({url: `http://localhost:3000/api/v1/products/${parsedBody.data.id}`},
                function(err, response, body){
                  if (err) t.fail(err);
                  let parsedBody = JSON.parse(body);

                  t.equal(response.statusCode, 404, `The product we are trying to delete does not exist`);
                  t.ok(parsedBody.errors instanceof Array, 'Errors attribute must exist because the product does not exist [del]');
                }
              );
              //Testing getting a non existing resource
              request.get({url: `http://localhost:3000/api/v1/products/${parsedBody.data.id}`},
                function(err, response, body){
                  if (err) t.fail(err);
                  let parsedBody = JSON.parse(body);

                  t.equal(response.statusCode, 404, "The product we are trying to retrieve does not exist");
                  t.ok(parsedBody.errors instanceof Array, 'Errors attribute must exist because the product does not exit [get]');
                }
              );

              //Testing updating non existing resource
              request.patch({url: `http://localhost:3000/api/v1/products/${parsedBody.data.id}`, form: {name:'a', slug:'b'}},
                function(err, response, body){
                  if (err) t.fail(err);
                  let parsedBody = JSON.parse(body);

                  t.equal(response.statusCode, 404, "The product we are trying to update does not exist");
                  t.ok(parsedBody.errors instanceof Array, 'Errors attribute must exist because the prodct does not exist [update]');
                }
              );
            }
          );
      });

    });
  });

  //Testing list
  request.get({url: `http://localhost:3000/api/v1/products`},
    function(err, response, body){
      if (err) t.fail(err);
      let parsedBody = JSON.parse(body);

      t.ok(parsedBody.data instanceof Array, 'Data attribute must be an instance of Array');
    }
  );

  t.end();
});

test("Creating product with errors", function(t){

  let slug = 'test-product-with-errors';

  request.post({url: 'http://localhost:3000/api/v1/products',
    form: {slug: slug}
  }, function(err, response, body){

    if (err) t.fail(err);
    let parsedBody = JSON.parse(body);

    t.equal(response.statusCode, 400, "Status code should be 400 'bad request' in products");
    t.ok(parsedBody.errors instanceof Array, 'Errors attribute must exist because the name parameter is missing in products');

  });

  t.end();
});