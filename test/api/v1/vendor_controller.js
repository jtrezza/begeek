'use strict'
process.env.NODE_ENV = 'test';

const test = require('tape'),
      controllers = require('../../../controllers'),
      request = require('request');

test("Testing create, update, list and delete in vendors", function(t){

  let name = 'Test vendor',
      slug = 'test-vendor';

  //Testing create operation
  request.post({url: 'http://localhost:3000/api/v1/vendors',
    form: {name: name}
  }, function(err, response, body){

    if (err) t.fail(err);
    let parsedBody = JSON.parse(body);

    t.equal(response.statusCode, 201, "Status code should be 201 'created' for vendor");
    t.ok(Number.isInteger(parsedBody.data.id), 'New ID must be an Integer');
    t.equal(parsedBody.data.name, name, 'Field name has been saved properly');

    //Testing update inside the create callback
    let new_name = 'New Name'
    request.patch({url: `http://localhost:3000/api/v1/vendors/${parsedBody.data.id}`,
      form: {name: new_name}
    }, function(err, response, body){
      if (err) t.fail(err);
      let parsedBody = JSON.parse(body);

      t.equal(response.statusCode, 200, "Status code should be 200 'OK' on update");
      t.equal(parsedBody.data.name, new_name, 'Field name has been updated properly');

      //Testing get inside the update callback
      request.get({url: `http://localhost:3000/api/v1/vendors/${parsedBody.data.id}`},
        function(err, response, body){
          if (err) t.fail(err);
          let parsedBody = JSON.parse(body);

          t.equal(response.statusCode, 200, "Vendor retrieved successfully");
          t.equal(parsedBody.data.name, new_name, 'Field name has been retrieved properly');

          //Testing delete inside the get callback
          request.del({url: `http://localhost:3000/api/v1/vendors/${parsedBody.data.id}`},
            function(err, response, body){
              if (err) t.fail(err);

              t.equal(response.statusCode, 200, "Status code should be 200 'OK' on delete");

              //Testing delete on a non existing resource
              request.del({url: `http://localhost:3000/api/v1/vendors/${parsedBody.data.id}`},
                function(err, response, body){
                  if (err) t.fail(err);
                  let parsedBody = JSON.parse(body);

                  t.equal(response.statusCode, 404, `The vendor we are trying to delete does not exist`);
                  t.ok(parsedBody.errors instanceof Array, 'Errors attribute must exist because the vendor does not exist [del]');
                }
              );
              //Testing getting a non existing resource
              request.get({url: `http://localhost:3000/api/v1/vendors/${parsedBody.data.id}`},
                function(err, response, body){
                  if (err) t.fail(err);
                  let parsedBody = JSON.parse(body);

                  t.equal(response.statusCode, 404, "The vendor we are trying to retrieve does not exist");
                  t.ok(parsedBody.errors instanceof Array, 'Errors attribute must exist because the vendor does not exist [get]');
                }
              );

              //Testing updating non existing resource
              request.patch({url: `http://localhost:3000/api/v1/vendors/${parsedBody.data.id}`, form: {name:'a'}},
                function(err, response, body){
                  if (err) t.fail(err);
                  let parsedBody = JSON.parse(body);

                  t.equal(response.statusCode, 404, "The vendor we are trying to update does not exist");
                  t.ok(parsedBody.errors instanceof Array, 'Errors attribute must exist because the vendor does not exist [update]');
                }
              );
            }
          );
      });

    });
  });

  //Testing list
  request.get({url: `http://localhost:3000/api/v1/vendors`},
    function(err, response, body){
      if (err) t.fail(err);
      let parsedBody = JSON.parse(body);

      t.ok(parsedBody.data instanceof Array, 'Data attribute must be an instance of Array');
    }
  );

  t.end();
});

test("Creating vendor with errors", function(t){

  request.post({url: 'http://localhost:3000/api/v1/vendors',
    form: {}
  }, function(err, response, body){

    if (err) t.fail(err);
    let parsedBody = JSON.parse(body);

    t.equal(response.statusCode, 400, "Status code should be 400 'bad request' on vendor");
    t.ok(parsedBody.errors instanceof Array, 'Errors attribute must exist because the name parameter is missing in vendors');

  });

  t.end();
});