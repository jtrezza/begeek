'use strict'

const test = require('tape'),
  controllers = require('../../../controllers'),
  fs = require('fs'),
  path = require('path'),
  util = require('util'),
  async = require('async'),
  request = require('request');

test("Test creating images", function(t){

  async.waterfall([
    function(callback){
      //Creating a new product
      request.post({url: 'http://localhost:3000/api/v1/products',
        form: {name: 'foo', slug: 'bar'}
      }, function(err, response, body){

        if (err) {
          t.fail(err);
          callback(err);
        }

        let parsedProduct = JSON.parse(body);
        callback(null, parsedProduct);
      });

    },
    function(parsedProduct, callback){

      let title = 'Test title',
        description = 'Test description';

      let formData = {
        product_id: parsedProduct.data.id,
        title: title,
        description: description,
        image: fs.createReadStream(path.join(__dirname, '..', '..', 'images', 'abstract.jpeg'))
      };
      //Creating a new Product Image
      request.post({url: `http://localhost:3000/api/v1/products/${formData.product_id}/images`,
        formData: formData}, function(err, response, body){

          if (err) {
            t.fail(err);
            callback(err);
          }
          let parsedImage = JSON.parse(body);

          t.equal(response.statusCode, 201, "Status code should be 201 'created on productImage'");
          t.ok(Number.isInteger(parsedImage.data.id), 'New ID must be an Integer on new productImage');
          t.equal(parsedImage.data.title, title, 'Field title has been saved properly');

          callback(null, parsedImage);
      });
    },
    function(parsedImage, callback){

      let title = 'Updated title';
      let description = 'Updated description';
      //Updating a Product Image
      request.patch({url: `http://localhost:3000/api/v1/productimages/${parsedImage.data.id}`,
      form: {title: title, description: description}}, function(err, response, body){

          if (err) {
            t.fail(err);
            callback(err);
          }

          let parsedImageBody = JSON.parse(body);

          t.equal(response.statusCode, 200, "Status code should be 200 'OK' on update");
          t.equal(parsedImageBody.data.title, title, 'Field title has been updated properly on product image');
          t.equal(parsedImageBody.data.description, description, 'Field description has been updated properly on product image');

          callback(null, parsedImageBody);
        });
    }
  ]);

  t.end();
});