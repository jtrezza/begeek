'use strict';

module.exports = function(sequelize, DataTypes) {

  let Category = sequelize.define('Category', {
    parent_id: {
      type: DataTypes.INTEGER,
      validate: {
        isInt: {
          msg: "The field 'parent_id' must be an Integer"
        }
      }
    },
    name: {type: DataTypes.STRING, validate: {max: 255}},
    slug: {type: DataTypes.STRING, validate: {max: 255}}, //TODO: uniq index in migrations
    description: DataTypes.TEXT,
    short_description: {type: DataTypes.STRING, validate: {max: 255}},
    seo_title: {type: DataTypes.STRING, validate: {max: 255}},
    seo_description: {type: DataTypes.STRING, validate: {max: 255}},
    visibility: {type: DataTypes.BOOLEAN, validate: {is: /^(true|false)$/i}}
  }, {
    classMethods: {
      associate: function(models) {
        Category.belongsToMany(models.Product, {through: 'product_category', foreignKey: 'category_id'});
      }
    },
    tableName: 'categories',
    paranoid: true,
    underscored: true
  });

  return Category;
};