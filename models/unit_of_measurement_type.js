'use strict';
module.exports = function(sequelize, DataTypes) {
    var UnitOfMeasurementType = sequelize.define('UnitOfMeasurementType', {
        name: DataTypes.STRING
    }, {
        classMethods: {
            associate: function(models) {
                // associations can be defined here
            }
        },
        tableName: 'units_of_measurement_types',
        paranoid: true
    });
    return UnitOfMeasurementType;
};