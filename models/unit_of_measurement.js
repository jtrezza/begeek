'use strict';
module.exports = function(sequelize, DataTypes) {
    var UnitOfMeasurement = sequelize.define('UnitOfMeasurement', {
        type_id: DataTypes.INTEGER,
        name: DataTypes.STRING
    }, {
        classMethods: {
            associate: function(models) {
                // associations can be defined here
            }
        },
        tableName: 'units_of_measurement',
        paranoid: true
    });
    return UnitOfMeasurement;
};