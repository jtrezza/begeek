'use strict';
module.exports = function(sequelize, DataTypes) {
  var Gender = sequelize.define('Gender', {
    code: DataTypes.CHAR(1),
    name: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    },
    tableName: 'genders'
  });
  return Gender;
};