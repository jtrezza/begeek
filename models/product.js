'use strict';
module.exports = function(sequelize, DataTypes) {
  let Product = sequelize.define('Product', {
    sku: {type: DataTypes.STRING, unique: true, validate: {max: 255}}, //TODO: uniq index in migrations
    name: {type: DataTypes.STRING, validate: {max: 255}},
    slug: {type: DataTypes.STRING, unique: true, validate: {max: 255}}, //TODO: uniq index in migrations
    vendor_id: DataTypes.INTEGER,
    description: DataTypes.TEXT,
    short_description: {type: DataTypes.STRING, validate: {max: 255}},
    price: {type: DataTypes.INTEGER, validate: {isInt: {msg: 'The price must be an integer'}}},
    sale_price: {type: DataTypes.INTEGER, validate: {isInt: {msg: 'The sale price must be an integer'}}},
    total_stock: DataTypes.INTEGER,
    visibility: {type: DataTypes.BOOLEAN, validate: {is: /^(true|false)$/i}},
    seo_title: {type: DataTypes.STRING, validate: {max: 255}},
    seo_description: {type: DataTypes.STRING, validate: {max: 255}},
    unit_of_measurement_id: DataTypes.INTEGER,
    weight: DataTypes.FLOAT, //To calculate shipping cost
    combinations: DataTypes.JSON,
    reviews: DataTypes.JSON
  }, {
    classMethods: {
      associate: function(models) {
        Product.hasMany(models.ProductImage);
        Product.belongsToMany(models.Category, {through: 'product_category', foreignKey: 'product_id'});
      }
    },
    tableName: 'products',
    paranoid: true,
    underscored: true
  });
  return Product;
};