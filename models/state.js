'use strict';
module.exports = function(sequelize, DataTypes) {
  var State = sequelize.define('State', {
    code: DataTypes.STRING,
    name: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    },
    tableName: 'states'
  });
  return State;
};