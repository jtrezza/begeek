'use strict';
module.exports = function(sequelize, DataTypes) {

  let ProductImage = sequelize.define('ProductImage', {
    product_id: {
      type: DataTypes.INTEGER,
      validate: {isInt: {msg: "The field 'product_id' must be an Integer"}}
    },
    filename: {type: DataTypes.STRING, validate: {max: 255}},
    title: {type: DataTypes.STRING, validate: {max: 255}},
    description: {type: DataTypes.STRING, validate: {max: 255}},
    order: DataTypes.INTEGER,
    main: DataTypes.BOOLEAN
  }, {
    classMethods: {
      associate: function(models) {
        ProductImage.belongsTo(models.Product);
      }
    },
    tableName: 'product_images',
    underscored: true
  });

  return ProductImage;
};