'use strict';
module.exports = function(sequelize, DataTypes) {
  var User = sequelize.define('User', {
    names: DataTypes.STRING,
    surnames: DataTypes.STRING,
    gender_id: DataTypes.INTEGER,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    role_id: DataTypes.INTEGER,
    remember_token: DataTypes.STRING,
    active: DataTypes.BOOLEAN
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    },
    tableName: 'users',
    paranoid: true
  });
  return User;
};