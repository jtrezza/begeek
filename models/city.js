'use strict';
module.exports = function(sequelize, DataTypes) {
  var City = sequelize.define('City', {
    code: DataTypes.STRING,
    state_id: DataTypes.INTEGER,
    name: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    },
    tableName: 'cities'
  });
  return City;
};